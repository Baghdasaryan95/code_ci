<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<style type="text/css">
		#register{
			width: 400px;
			height: auto;
			border: 3px solid gray;
			border-radius: 30px;
			text-align: center;
			margin: 50px 0 0 50px;
		}
		#login{
			display: inline-block;
			width: 300px;
			height: auto;
			border: 3px solid gray;
			border-radius: 30px;
			text-align: center;
			margin: 50px 0 0 50px;

		}
	</style>
</head>
<body>
	<h1>join our family</h1>
	<!-- validationi errorneri tpum -->
	<!-- <?php //echo validation_errors(); ?> -->
	<form id="login" class="form-inline" method="post" action="<?= base_url('user/login')?>">

		<input class="form-control" type="text" name="email" placeholder="Լոգին"><br><br>
		<input class="form-control" type="password" name="parol" placeholder="գաղտնաբառ"><br>

		<button>Log in</button>

		<?php if (isset($log_message)): ?>
			<?php echo $log_message ?>
		<?php endif ?>

		
	</form>


	<form id="register" class="form-inline" method="post" action="<?= base_url('user/signup')?>">
		<label>registration form</label><br><br>
	
		<input class="form-control" type="text" name="first" placeholder="enter your name" value="<?php echo set_value('first'); ?>"/><br><br>
		 <?php echo form_error ('first'); ?>


		<input class="form-control" type="text" name="surname" placeholder="enter your surname" value="<?php echo set_value('surname'); ?>"/><br><br>
		 <?php echo form_error ('surname'); ?>


		<input class="form-control" type="text" name="email" placeholder="enter your email" value="<?php echo set_value('email'); ?>"><br><br>
		 <?php echo form_error ('email'); ?>


	
		<input class="form-control" type="text" name="age" placeholder="enter your age" value="<?php echo set_value('age'); ?>"><br><br>
		 <?php echo form_error ('age'); ?>

		<input class="form-control" type="password" name="password" placeholder="enter your password" value="<?php echo set_value('password'); ?>"><br><br>
		 <?php echo form_error ('password'); ?>

		<input class="form-control" type="password" name="passconf" placeholder="enter your confirm password" value="<?php echo set_value('passconf'); ?>"><br><br>
		 <?php echo form_error ( 'passconf' ); ?>

		<button class="btn btn-success">Send</button>

		<?php  
		if (isset($message)) {
			echo $message;
		}
		?>

	</form>

	

</body>
</html>