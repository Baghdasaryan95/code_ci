<?php

	class User extends CI_Controller{

		function __construct(){

			parent::__construct();

			$lbs = array(
				'form_validation',
				'session'
			);

			$this->load->library($lbs);
			$this->load->model('Users_model'); 
		}

		public function index(){

			$this->load->view('register');

		}
		function signup(){	
//inputi valuen-------			
			// $a = $this->input->post('first');
			// echo $a;
//__formaneri validacia________________	
			

//validacion kanoneri haytararum 
			$this->form_validation->set_rules('first','Անուն','required|alpha|max_length[15]',array('required' => '%s Դաշտը լրացված չէ','alpha'=>'մուտքագրեք միայն տառեր','max_length' => "Մաքսիմում 15 տառ"));

			$this->form_validation->set_rules('surname', 'Ազգանուն', 'required|alpha|max_length[15]',array('required' => '%s Դաշտը լրացված չէ','alpha'=>'մուտքագրեք միայն տառեր','max_length' => "Մաքսիմում 15 տառ"));

			$this->form_validation->set_rules('email', 'էլ.հասցե', 'required|valid_email|is_unique[users.email]',array('required' => '%s Դաշտը լրացված չէ','is_unique' => 'կրկնվող էլ.հասցե'));

			$this->form_validation->set_rules('age', 'տարիք', 'is_natural',array('is_natural' => '%s Դաշտը լրացված չէ'));

			$this->form_validation->set_rules('password', 'գաղտնաբառ', 'required|min_length[6]', array('required' => '%s Դաշտը լրացված չէ', 'min_length' => "Առնվազն 6 սիմվոլ"));

			$this->form_validation->set_rules('passconf', 'գաղտնաբառ', 'required|matches[password]', array('required' => '%s Դաշտը լրացված չէ','matches' => "%s սխալ է"));
			



			

//ete form_validation ashxatacneluc sxal e texapoxel register
			if ($this->form_validation->run()==FALSE) {
				$this->load->view('register');
			}else{
				
				$this->Users_model->add_user(); //load aneluc heto tvyalnery vercnume Users_model.phpic



				$this->load->view('register',['message'=>'<div class="alert alert-success text-center">դուք գրանցվեցիք!</div>']);
			}

		}

		function login(){
			
			$r = $this->Users_model->login_check();

			if ($r === FALSE ) {
				
				$this->load->view('register',['log_message'=>'<div class="alert alert-danger text-center">sxal tvyalenr</div>']);

			}else{

				$this->session->set_userdata('user',$r); // pahel enq userin sessionum

				redirect('user/profile'); // texapoxum enq es hasceov (link)

			}

	}

	function profile(){

		if($this->session->has_userdata('user')){ // ete ka user sessioni mej apa nor texapoxel

			$this->load->view('profile');
		}else{

			show_404();
		}

	}
}




?>